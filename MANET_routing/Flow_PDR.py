from xml.etree import ElementTree as ET
import sys
import matplotlib.pyplot as pylab
from glob import glob
import os
import re

data = glob("*/*.flowmon")
data.sort();
print(data)
pdr_file = open("pdr_file.data","a")
pdr_file.seek(0) #ensure you're at the start of the file..
#first_char = pdr_file.read(1) #get the first character
if os.stat("pdr_file.data").st_size == 0:
    pdr_file.write("n AODV DSDV OLSR\n")#first character is the empty string..
else:
    pdr_file.seek(0) #first character wasn't empty, return to start of file.
     #use file now

for name in data:
	et=ET.parse(name)
	bitrates=[]
	losses=[]
	delays=[]
	jitter=[]
	totPackets=0
	lostPackets=0
	pdr=0
	for flow in et.findall("FlowStats/Flow"):
		for tpl in et.findall("Ipv4FlowClassifier/Flow"):
			if tpl.get('flowId')==flow.get('flowId'):
				break
		if tpl.get('destinationPort')=='654':
			continue
		losses.append(int(flow.get('lostPackets')))
		
		lostPackets=lostPackets+int(flow.get('lostPackets'))
		#print(lostPackets)
		totPackets=totPackets+int(flow.get('txPackets'))
		#print(totPackets)
		rxPackets=int(flow.get('rxPackets'))
		if rxPackets==0:
			bitrates.append(0)
		else:
			t0=float(flow.get('timeFirstRxPacket')[:-2])
			t1=float(flow.get("timeLastRxPacket")[:-2])
			duration=(t1-t0)*1e-9
			bitrates.append(8*long(flow.get("rxBytes"))/duration*1e-3)
			delays.append(float(flow.get('delaySum')[:-2])*1e-9/rxPackets)
			jitter.append(float(flow.get('jitterSum')[:-2])*1e-9/rxPackets)

	if("AODV" in name):
		result = re.search('n(.*),t', name)
		pdr_file.write(result.group(1)+ " ")

	pdr = float(lostPackets)/totPackets*100	
	print(pdr)
	pdr_file.write(str(100-pdr)+" ")

	if("OLSR" in name):
		pdr_file.write("\n")

	pylab.subplot(711)
	pylab.hist(bitrates,bins=40)
	pylab.xlabel("Flow Bit Rates (b/s)")
	#pylab.ylabel(" Number of Flows")

	pylab.subplot(713)
	pylab.hist(losses,bins=40)
	pylab.xlabel("No of Lost Packets")
	#pylab.ylabel(" Number of Flows")

	pylab.subplot(715)
	pylab.hist(delays,bins=10)
	pylab.xlabel("Delay in Seconds")
	pylab.ylabel(" Number of Flows")

	pylab.subplot(717)
	pylab.hist(jitter,bins=10)
	pylab.xlabel("Jitter in Seconds")
	#pylab.ylabel(" Number of Flows")

	pylab.subplots_adjust(hspace=0.4)
	pylab.savefig(name[:-8]+"_Flow.pdf")

	pylab.clf()