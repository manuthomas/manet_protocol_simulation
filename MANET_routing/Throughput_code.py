import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from glob import glob
import re
import os

data = glob("*/*.data")
data.sort();

thru_file = open("Thru_file.val","a")
thru_file.seek(0) #ensure you're at the start of the file..
#first_char = pdr_file.read(1) #get the first character
if os.stat("Thru_file.val").st_size == 0:
    thru_file.write("n AODV DSDV OLSR\n")#first character is the empty string..
else:
    thru_file.seek(0) #first character wasn't empty, return to start of file.
     #use file now

for name in data:
	
	if("AODV" in name):
		result = re.search('n(.*),t', name)
		thru_file.write(result.group(1)+ " ")		

	df = pd.read_csv(name,header=None,sep=",")
	throughput = df.iloc[:,1].mean()
	thru_file.write(str(throughput)+ " ")

	if("OLSR" in name):
		thru_file.write("\n")